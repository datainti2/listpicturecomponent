import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SampleComponent } from './src/sample.component';
import { SampleDirective } from './src/sample.directive';
import { SamplePipe } from './src/sample.pipe';
import { SampleService } from './src/sample.service';
import { MaterialModule } from '@angular/material';
import { BaseWidgetListPictureComponent } from './src/base-widget/base-widget.component';

export * from './src/sample.component';
export * from './src/sample.directive';
export * from './src/sample.pipe';
export * from './src/sample.service';
export * from './src/base-widget/base-widget.component';


@NgModule({
  imports: [
    CommonModule,
    MaterialModule
  ],
  declarations: [
    SampleComponent,
    SampleDirective,
    SamplePipe,
    BaseWidgetListPictureComponent
  ],
  exports: [
    SampleComponent,
    SampleDirective,
    SamplePipe,
    BaseWidgetListPictureComponent
  ]
})
export class ListPictureModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ListPictureModule,
      providers: [SampleService]
    };
  }
}
