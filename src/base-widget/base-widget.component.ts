import { Component } from '@angular/core';

@Component({
  selector: 'app-base-widget',
  template: '<div class="col-md-10 col-md-offset-1"><md-grid-list cols="6"><md-grid-tile *ngFor="let img of images"><img src="{{img.image}}" class="img-rounded" alt="Cinque Terre" width="154" height="136"></md-grid-tile></md-grid-list></div>',
  styleUrls: ['./base-widget.component.css']
})
export class BaseWidgetListPictureComponent {

    public images: Array<any> = [
    {"image":"../assets/img/noimg.png"}, 
    {"image":"../assets/img/noimg.png"},
    {"image":"../assets/img/noimg.png"},
    {"image":"../assets/img/noimg.png"},
    {"image":"../assets/img/noimg.png"},
    {"image":"../assets/img/noimg.png"},
    {"image":"../assets/img/noimg.png"}, 
    {"image":"../assets/img/noimg.png"},
    {"image":"../assets/img/noimg.png"},
    {"image":"../assets/img/noimg.png"},
    {"image":"../assets/img/noimg.png"},
    {"image":"../assets/img/noimg.png"},
    {"image":"../assets/img/noimg.png"},
    {"image":"../assets/img/noimg.png"},
    {"image":"../assets/img/noimg.png"},
    {"image":"../assets/img/noimg.png"},
    {"image":"../assets/img/noimg.png"},
    {"image":"../assets/img/noimg.png"}];

}
