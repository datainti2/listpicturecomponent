import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseWidgetListPictureComponent } from './base-widget.component';

describe('BaseWidgetListPictureComponent', () => {
  let component: BaseWidgetListPictureComponent;
  let fixture: ComponentFixture<BaseWidgetListPictureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseWidgetListPictureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseWidgetListPictureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
